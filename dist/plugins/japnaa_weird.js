"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const plugin_js_1 = __importDefault(require("../main/bot/plugin/plugin.js"));
const actions_1 = require("../main/bot/actions/actions");
/**
 * The weirder side of JaPNaABot
 */
class JapnaaWeird extends plugin_js_1.default {
    lolRegexp = /(\W|^)(([l1|\\!/\uff4c]|(\ud83c\uddf1))+)+[\W_]*((h|w)*([a@&\*eiouy0.=]|(\ud83c\udd7e\ufe0f)|(\ud83c\uddf4))+(h|w)*)[\W_]*([l1|\\!/\uff4c]|(\ud83c\uddf1))+(\W|$)/i;
    // note: original (aggressive) lol detection: /(\s*[l|\\!/]+\s*)+\W*((h|w)*([aeiouy0.=]|(?!\s)\W)+(h|w)*)\W*[l|\\!/]+/i
    l$wlRegexp = (() => {
        const letter12 = "([LlI1|\\\\!/\\uff4c\\u029f\\u03b9\\u0627\\u13de\\u216c\\u217c\\uff2c\\uff4c]|(\\ud83c\\uddf1))";
        const letter5 = "([eEе3\\uff45\\u00c8-\\u00cb\\u00e8-\\u00eb\\u0112-\\u011b\\u0395\\u0415\\u0435\\u0435\\u13ac\\uff25\\uff45]|(\\ud83c\\uddea))";
        const letter23 = "([wuｗ\\u0461\\u13b3\\uff37\\uff57]|[vV]{2}|(\\ud83c\\uddfc))";
        const russian = "ЛЮЉ";
        const whitespace = "[\\W_]*";
        const absoluteValueOf = "(the)?" + whitespace + "absolute" + whitespace + "(value)?" + whitespace + "(of)?" + whitespace + letter5 + whitespace + letter23;
        return new RegExp(`${russian}|${letter12}${whitespace}${letter5}${whitespace}${letter23}${whitespace}${letter12}|${absoluteValueOf}`, "g");
    })();
    // note: original (including i as L) l$wlRegexp: /.(ЛЮЉ)|(([il1|\\!/\uff4c]|(\ud83c\uddf1))[\W_]*([e3\uff45]|(\ud83c\uddea))[\W_]*((vv)|(\ud83c\uddfc)|[wuｗ])[\W_]*([il1|\\!/\uff4c]|(\ud83c\uddf1))[\W_]*)|((the[\W_]*)?absolute[\W_]*(value[\W_]*)?(of[\W_]*)?([e3\uff45]|(\ud83c\uddea))[\W_]*((vv)|(\ud83c\uddfc)|[wuｗ]))/gi;
    goodBotRegexp = /(\s|^)good bots?(\s|$)/i;
    badBotRegexp = /(\s|^)bad bots?(\s|$)/i;
    noMessageRegexp = /^\s*(no\s*)+$/;
    constructor(bot) {
        super(bot);
        this.pluginName = "japnaaweird";
    }
    /**
     * Tetris is a racing game.
     */
    *tetris(event) {
        yield "**Tetris is a " + (event.arguments || "racing") + " game**";
    }
    /**
     * JaP is kewl
     */
    *jap(event) {
        yield {
            embeds: [{
                    color: this.bot.config.themeColor,
                    description: "**JaP is " + (event.arguments || "kewl") + "**"
                }]
        };
    }
    /**
     * ebola your parabola
     */
    *your() {
        yield "parabola";
    }
    *what_should_i_wear() {
        yield "It's -273.15°C in space, so you should wear:\n" +
            "  - nothing on your head\n" +
            "  - nothing on your torso\n" +
            "  - nothing on your legs\n" +
            "  - nothing on your feet\n" +
            "  - nothing on your hands\n" +
            "You cannot save this outfit by telling to me `save outfit`!";
    }
    /**
     * Listens for messages with 'lol' and deviations
     */
    async *onmessageHandler_lol(event, eventControls) {
        if (!await this._isUserMessage(this.bot, event)) {
            return;
        }
        const numL$wl = this._countL$wl(event.message);
        if (numL$wl) {
            yield "no ".repeat(numL$wl);
            // ignore commands with matching l$wl
            if (event.precommandName) {
                eventControls.preventSystemNext();
            }
        }
        else if (!event.precommandName) {
            if (this.lolRegexp.test(event.message)) {
                // ^ contains valid 'lol' and is not command
                yield "lol";
            }
            if (this.goodBotRegexp.test(event.message)) {
                yield new actions_1.ReplyReact("\ud83d\ude04");
            }
            if (this.badBotRegexp.test(event.message)) {
                yield new actions_1.ReplyReact("\ud83d\ude26");
            }
        }
    }
    /**
     * The messageEditHandler handles the case when a user edits a message to
     * have more l-words than before. This function adds 'no's to the previous
     * response or creates a message with the appropriate number of 'no's.
     */
    async messageEditHandler(oldMessage, newMessage) {
        const oldCount = oldMessage.content ? this._countL$wl(oldMessage.content) : 0;
        const newCount = newMessage.content ? this._countL$wl(newMessage.content) : 0;
        const delta = newCount - oldCount;
        if (delta <= 0) {
            return;
        }
        if (!('messages' in newMessage.channel)) {
            return;
        }
        const messagesAfter = await newMessage.channel.messages.fetch({
            after: newMessage.id,
            limit: 3
        });
        let noMessage;
        for (const [id, message] of messagesAfter) {
            if (message.author.id === this.bot.client.id && this.noMessageRegexp.test(message.content)) {
                noMessage = message;
                break;
            }
        }
        if (noMessage) {
            const currCount = noMessage.content.match(/no/ig)?.length || 0;
            noMessage.edit("no ".repeat(currCount + delta));
        }
        else {
            this.bot.client.send(newMessage.thread?.id || newMessage.channelId, {
                content: "no ".repeat(delta),
                flags: "SuppressNotifications"
            });
        }
    }
    /**
     * The messageDeleteHandler handles the case when a user deletes a 'no'
     * message sent by the bot. This function deletes the user's last message
     * containing l-words. (If the number of no-s and l-words don't add up,
     * will also send an additional no message.)
     */
    async messageDeleteHandler(message) {
        // filter: messages from the bot only
        if (!message.author || message.author.id !== this.bot.client.id) {
            return;
        }
        // filter: only 'no' messages
        if (!message.content || !this.noMessageRegexp.test(message.content)) {
            return;
        }
        const messagesBefore = await message.channel.messages.fetch({
            before: message.id,
            limit: 3
        });
        const targetCount = message.content.match(/no/ig)?.length || 0;
        let closestLwordCount = 0;
        let matchingMessage = null;
        for (const [_id, message] of messagesBefore) {
            const count = this._countL$wl(message.content);
            if (Math.abs(targetCount - count) < Math.abs(targetCount - closestLwordCount)) {
                closestLwordCount = count;
                matchingMessage = message;
            }
        }
        if (!matchingMessage) {
            this.bot.client.send(message.channelId, {
                content: "no ".repeat(targetCount),
                flags: "SuppressNotifications"
            });
            return;
        }
        await matchingMessage.delete();
        const unaccountedLwords = targetCount - closestLwordCount;
        if (unaccountedLwords > 0) {
            this.bot.client.send(message.channelId, {
                content: "no ".repeat(unaccountedLwords),
                flags: "SuppressNotifications"
            });
        }
    }
    _countL$wl(str) {
        let i = 0;
        for (let match; match = this.l$wlRegexp.exec(str); i++) {
            if (str.slice(match.index - 1, match.index + 3).toLowerCase() === "view") {
                i--;
            }
        }
        return i;
    }
    async _isUserMessage(bot, event) {
        const user = await bot.client.getUser(event.userId);
        return Boolean(user && !user.bot);
    }
    _start() {
        this._registerDefaultCommand("jap", this.jap);
        this._registerDefaultCommand("tetris", this.tetris);
        this._registerDefaultCommand("your", this.your);
        this._registerDefaultCommand("what should i wear", this.what_should_i_wear, {
            help: {
                description: "Replies with a message in memory of 'outfit based on weather bot.' If JaPNaA feels like it some day, they may reimplement the behavior in JaPNaABot."
            }
        });
        this._registerMessageHandler(this.onmessageHandler_lol);
        this.messageEditHandler = this.messageEditHandler.bind(this);
        this.bot.client.client.on("messageUpdate", this.messageEditHandler);
        this.messageDeleteHandler = this.messageDeleteHandler.bind(this);
        this.bot.client.client.on("messageDelete", this.messageDeleteHandler);
    }
    _stop() {
        this.bot.client.client.off("messageUpdate", this.messageEditHandler);
    }
}
exports.default = JapnaaWeird;
