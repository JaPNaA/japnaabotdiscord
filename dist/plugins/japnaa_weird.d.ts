import DiscordMessageEvent from "../main/bot/events/discordMessageEvent";
import BotPlugin from "../main/bot/plugin/plugin.js";
import Bot from "../main/bot/bot/bot";
import DiscordCommandEvent from "../main/bot/events/discordCommandEvent";
import { EventControls } from "../main/bot/events/eventHandlers";
import { ReplyReact } from "../main/bot/actions/actions";
/**
 * The weirder side of JaPNaABot
 */
declare class JapnaaWeird extends BotPlugin {
    private lolRegexp;
    private l$wlRegexp;
    goodBotRegexp: RegExp;
    badBotRegexp: RegExp;
    noMessageRegexp: RegExp;
    constructor(bot: Bot);
    /**
     * Tetris is a racing game.
     */
    tetris(event: DiscordCommandEvent): Generator<string, void, unknown>;
    /**
     * JaP is kewl
     */
    jap(event: DiscordCommandEvent): Generator<{
        embeds: {
            color: number;
            description: string;
        }[];
    }, void, unknown>;
    /**
     * ebola your parabola
     */
    your(): Generator<string, void, unknown>;
    what_should_i_wear(): Generator<string, void, unknown>;
    /**
     * Listens for messages with 'lol' and deviations
     */
    onmessageHandler_lol(event: DiscordMessageEvent, eventControls: EventControls): AsyncGenerator<string | ReplyReact, void, unknown>;
    /**
     * The messageEditHandler handles the case when a user edits a message to
     * have more l-words than before. This function adds 'no's to the previous
     * response or creates a message with the appropriate number of 'no's.
     */
    private messageEditHandler;
    /**
     * The messageDeleteHandler handles the case when a user deletes a 'no'
     * message sent by the bot. This function deletes the user's last message
     * containing l-words. (If the number of no-s and l-words don't add up,
     * will also send an additional no message.)
     */
    private messageDeleteHandler;
    private _countL$wl;
    _isUserMessage(bot: Bot, event: DiscordMessageEvent): Promise<boolean>;
    _start(): void;
    _stop(): void;
}
export default JapnaaWeird;
